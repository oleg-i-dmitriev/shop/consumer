package com.dmitriev.consumer.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.SimpleMessageConverter;

import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import java.util.List;

@Configuration
@EnableJms
public class MqConfiguration {
    @Value("${activemq.url}")
    private String activemqUrl;

    @Value("${activemq.user}")
    private String activemqUser;

    @Value("${activemq.password}")
    private String activemqPassword;


    @Bean
    public MessageConverter messageConverter() {
        return new SimpleMessageConverter();
    }

    @Bean
    @Primary
    public ConnectionFactory jmsConnectionFactory() {
        var connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(activemqUrl);
        connectionFactory.setUserName(activemqUser);
        connectionFactory.setPassword(activemqPassword);
        connectionFactory.setTrustedPackages(List.of("*"));

        return connectionFactory;
    }

    @Bean
    @Primary
    public JmsTemplate jmsTemplate() {
        var jmsTemplate = new JmsTemplate();
        jmsTemplate.setConnectionFactory(jmsConnectionFactory());
        jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);

        return jmsTemplate;
    }
}
